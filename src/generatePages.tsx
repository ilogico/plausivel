import rollupNodeResolve from "@rollup/plugin-node-resolve";
import typescript from "@rollup/plugin-typescript";
import { promises as fs } from "fs";
import path from "path";
import { h } from "preact";
import render from "preact-render-to-string";
import { rollup } from "rollup";
import { fileURLToPath } from "url";
import Index from "./index.js";
import type Page from "./Page.js";

const objDir = path.dirname(fileURLToPath(import.meta.url));
const srcDir = path.resolve(objDir, "..", "src");
const outDir = path.resolve(objDir, "../public");

interface Assets {
  readonly modules: readonly string[];
  readonly styles: readonly string[];
}

async function generatePage(pathname: string, page: Page, assets: Assets) {
  const markup = (
    <html lang={page.lang}>
      <head>
        <meta charSet="utf-8" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1.0"
        />
        <link rel="icon" href="./icon.svg" />
        <title>plausivel - {page.title}</title>
        {assets.styles.map((href) =>
          <link key={href} rel="stylesheet" href={href} />
        )}
        {assets.modules.map((src) =>
          <script key={src} type="module" src={src} />
        )}
      </head>
      <body>
        {page.type === "markdown"
          ? <main dangerouslySetInnerHTML={{ __html: page.body }} />
          : <main>{page.body}</main>}
      </body>
    </html>
  );

  return fs.writeFile(pathname, "<!doctype html>" + render(markup));
}

async function generatePages() {
  const pageModules = await Promise.all(
    (await find(objDir, /\.page.js$/)).map((file) =>
      import(file).then((module) => module.default as Page)
    ),
  );
  const sources = await find(srcDir, /\.page.tsx?$/);
  const directories = new Map(sources.map((source) => {
    const dir = path.dirname(source);
    return [path.basename(dir), dir];
  }));

  if (directories.size !== pageModules.length) {
    throw new Error("Duplicate hrefs in pages");
  }

  const pages = await Promise.all(pageModules.map(async (page) => {
    const directory = directories.get(page.href)!;
    const modules = (page.modules || []).map((mod) =>
      mod.startsWith("/")
        ? path.resolve(srcDir, `.${mod}`)
        : path.resolve(directory, mod)
    );

    const styles = (page.type === "markdown"
      ? ["/theme.css", "highlight.js/styles/tomorrow-night-bright.css"]
      : ["/theme.css"]).concat(page.styles || []).map((style) => {
        if (style.startsWith(".")) {
          return path.join(directory, style);
        }
        if (style.startsWith("/")) {
          return path.resolve(srcDir, `.${style}`);
        }
        return path.resolve(objDir, "../node_modules", style);
      });

    return { page, modules, styles };
  }));

  pages.sort((a, b) =>
    new Date(b.page.date).getTime() - new Date(a.page.date).getTime()
  );

  const modules = pages.flatMap((p) => p.modules);

  const moduleBundle = await rollup({
    input: modules,
    plugins: [
      rollupNodeResolve({ browser: true }),
      typescript(),
    ],
  });

  const { output } = await moduleBundle.write({
    format: "es",
    dir: "public",
    sourcemap: true,
  });

  const moduleMap = new Map<string, string>();
  const styleMap = new Map<string, string>();
  const styleNames = new Set(["index.css"]);

  for (const chunk of output) {
    if ("isEntry" in chunk && chunk.facadeModuleId) {
      const { fileName, facadeModuleId } = chunk;
      moduleMap.set(facadeModuleId, fileName);
    }
  }

  await Promise.all(pages.map(async ({ page, modules, styles }) => {
    const stylesheets = await Promise.all(styles.map(async (style) => {
      let styleName = styleMap.get(style);
      if (styleName) return styleName;

      styleName = path.basename(style);
      if (styleNames.has(styleName)) {
        const ext = path.extname(styleName);
        const base = styleName.slice(0, -ext.length);
        let altName;
        let count = 2;
        do {
          altName = `${base}-${count++}${ext}`;
        } while (styleNames.has(altName));
        styleName = altName;
      }
      styleNames.add(styleName);
      styleMap.set(style, styleName);

      await fs.copyFile(
        style,
        path.join(outDir, styleName),
      );
      return styleName;
    }));

    await generatePage(
      path.join(outDir, `${page.href}.html`),
      page,
      {
        styles: stylesheets,
        modules: modules.map((m) => moduleMap.get(m)!),
      },
    );
  }));

  await fs.copyFile("src/index.css", path.join(outDir, "index.css"));

  await generatePage(
    path.join(outDir, "index.html"),
    {
      title: "Index",
      href: "",
      date: new Date().toISOString().slice(0, 10),
      body: <Index pages={pages.map(({ page }) => page)} />,
      type: "html",
      lang: "en",
    },
    {
      styles: ["./theme.css", "./index.css"],
      modules: [],
    },
  );
}

generatePages().catch((error) => {
  console.error(error);
  process.exit(1);
});

async function find(dir: string, pattern: RegExp): Promise<string[]> {
  const entries = await fs.readdir(dir, { withFileTypes: true });

  return ([] as string[]).concat(
    ...await Promise.all(entries.map((entry) => {
      const fullName = path.resolve(dir, entry.name);
      if (entry.isFile()) {
        return pattern.test(fullName) ? [path.resolve(dir, fullName)] : [];
      }

      if (entry.isDirectory()) {
        return find(path.resolve(dir, fullName), pattern);
      }

      return [];
    })),
  );
}
