<h1>Code Sandbox</h1>
<time datetime="2020-04-27">April 28, 2020</time>

I was using [codepen.io](https://codepen.io) the other day, when I realized the *iframe* used to display the results was actually fetching data from the Internet.
I was surprised because I assumed everything I needed for my code to work was the browser itself.

So I decided to do an experiment on how to do these sandboxes.

I can think of two ways of showing the *iframe* with the results.
1. Using service workers to deliver the page and assets.
This solution is the most flexible and it would even allow for writing a mock server.
2. Using data URLs. It's possible to use data URLs for the whole *iframe* as well as for the individual assets.

The second way so simple, I decided to try it.
I ended up with something like this:

<code-demo>

## Demo

### HTML
```html
<!doctype html>
<title>My CodeSandbox</title>
<label for="html">HTML</label>
<textarea id="html">Hello, </textarea>
<label for="css">CSS</label>
<textarea id="css">span { color: blue; }</textarea>
<label for="javascript">JavaScript</label>
<textarea id="javascript">document.body.appendChild(document.createElement('span')).appendChild(new Text('World!'));</textarea>
<iframe id="result"></iframe>
```

### CSS
```css
body {
    display: grid;
    grid-template-rows: repeat(3, 2rem 1fr) 2fr;
    justify-items: stretch;
    margin: 0;
    min-height: 100vh;
}
```

### JavaScript
```javascript
function makeDataURL(contentType, content) {
    return `data:${contentType};utf-8;base64,${btoa(content)}`;
}

const [html, css, js, result] = document.querySelectorAll('#html, #css, #javascript, #result');

function updateResult() {
    result.src = makeDataURL(
        'text/html',
        `<!doctype html>
        <link rel="stylesheet" href="${makeDataURL('text/css', css.value)}">
        <script type="module" src="${makeDataURL('text/javascript', js.value)}"></script>
        <body>${html.value}</body>
        `
    );
}
document.addEventListener(
    'input',
    updateResult,
    false,
);
updateResult();
```
</code-demo>

## Explanation
As you can see, the required code to make this work is very simple.
Of course, I'm not concerned with providing the best editing experience, so a *textarea* is sufficient.
I also don't care about debouncing the input, etc.

The only code that deserves an explanation is the `makeDataURL` function. Data URL have the form `data:,`.
Before the comma, we can add the content type and encodings. Since the data is being enconded in *base64* to avoid escaping anything, it's necessary to add it as a content encoding. I added the character encoding as well, otherwise the browser shows a console warning.

For encoding to *base64*, I used `btoa`, which is available in all browsers (and deno) and works fine with strings. It also has the counterpart `atob`, which decodes a *base64* string.

## By Product: a Custom Element for running Markdown code snippets
So, the objective was simply to understand how it could be done.
But then I realized I could use something simmilar to run the code in markdown code snippets, so I decided to do a custom element to do just that.
The current version of which is as follows:

```javascript
const template = document.createElement('template');
template.innerHTML = `
<style>
#container {
    display: grid;
}
iframe {
    iframe {
    min-height: 60vh;
    background: white;
    border: none;
}
</style>
<div id="container"><slot></slot><iframe part="document"></iframe></div>
`;

function makeDataURL(contentType, content) {
    return `data:${contentType};utf-8;base64,${btoa(content)}`;
}

class CodeDemo extends HTMLElement {
    constructor() {
        super();

        const shadowDOM = this.attachShadow({ mode: 'closed' });
        shadowDOM.appendChild(template.content.cloneNode(true));
        const iframe = shadowDOM.querySelector('iframe');

        const updateIframe = () => {
            const html = [...this.querySelectorAll('.language-html')].map(e => e.textContent).join('\n');
            const styles = [...this.querySelectorAll('.language-css')]
                .map(e => `<link rel="stylesheet" href="${makeDataURL('text/css', e.textContent)}">`)
                .join('\n');
            const modules = [...this.querySelectorAll('.language-js, .language-javascript')]
                .map(e => `<script type="module" src="${makeDataURL('text/javascript', e.textContent)}"></script>`)
                .join('\n');

            const doc = `
                <!doctype html>
                ${styles}
                ${modules}
                <body>${html}</body>
            `;
            iframe.src = makeDataURL('text/html', doc);
        };

        updateIframe();
        new MutationObserver(updateIframe).observe(this, { characterData: true, childList: true, subtree: true });
    }
}

customElements.define('code-demo', CodeDemo);
```

Since it's just a custom element, I can use it when writing Markdown (as long as I include the module which defines it, of course).
The code demonstration above is achieved by using the placing the snippets inside a `<code-demo />` tag.
So the result of editing the `textarea`s is shown in an `iframe` inside an `iframe` inside the shadow DOM of a custom element.

We can now reimplement everything inside the sandbox and continue the loop forever.
