import body from "./code-sandbox.md";
import type Page from "/Page.js";

const page: Page = {
  title: "Code Sandbox",
  href: "code-sandbox",
  date: "2020-04-27",
  lang: "en",
  type: "markdown",
  body,
  modules: ["/webcomponents/code-demo.ts"],
};

export default page;
