<h1>Mixing it up</h1>
<time datetime="2020-05-09">May 9, 2020</time>

Most programming languages I know only support single inheritance (C++ being the exception).
It's possible for a class to implement multiple interfaces, but it can only extend one other class.

However, many languages have the concept of `mixin` inheritance. While they don't allow you to extend
additional classes, they allow you `mixin` methods from other classes into the one you're writting.

JavaScript, in practice always allowed us to this, even if it's just by copying the methods from one
prototype to the other:

```javascript
class MyMixin {
    someMethod() {}
}

function applyMixin(klass) {
    for (const [name, method] of Object.entries(Object.getOwnPropertyDescriptors(MyMixin))) {
        if (name !== 'constructor') Object.defineProperty(klass.prototype, name, method);
    }
}
```

Of course, I didn't have to write a class just to hold the methods I wanted to apply, but this way
we get the correct property descriptors and the methods will not be enumerable. This even works with getters and setters.

But this approach is not very clean. I had to skip copying the constructor, which also means the mixin cannot
introduce any initialization logic in the `extending` class.

This approach is completely imperative, so we won't benefit from any intelisense and TypeScript will not be aware
of the new methods the class will acquire. Patching the class definition is possible, but can't be automated.
It works like this:

```typescript
class MyExtendedClass extends SomethingElse {
}
interface MyExtendedClass {
    someMethod(): void;
}
// After the interface augmentation
// TS even allows us to define the method with an assignment
MyExtendedClass.prototype = function someMethod() {}

```

The above code works because when we declare a class in TypeScript, we're actually defining two things.
One is the `MyExtendedClass` constructor. This is a value of type `function`.
The other is a type: the interface implemented by the objects constructed by the constructor.
TypeScript interfaces can be augmented. Multiple declarations of the same interface will be merged.

So in order to use this approach, we would have to do these augmentations whenever we applied a mixin.

But fortunately there's a much better approach.

JavaScript classes are functions and like all other functions, they can be passed around like any value.
This allows the creation of what might be called high order classes: function that receive classes and return classes:

```javascript
function myMixin(klass) {
    return class extends klass {
        someMethod() {}
    };
}

const MyClass = myMixin(class MyClass extends SomethingElse {
    whatever() {}
});
```

This is solving the problem the other way around. My class only extends one class, but it can also be extended,
so we make a factory function that makes classes on demand.

Now, the resulting class is not the class I wrote, but if that's important, I can always apply the mixin to
the class I'm extending instead:
```javascript
class MyClass extends myMixin(SomethingElse) {}
```

I still end up with a class inheriting all the methods I need.

As a bonus, this approach works very well with TypeScript:
```typescript
function myMixin<C extends new (...args: any[]) => any>(klass: C) {
    return class extends klass {
        constructor(...args: any[]) {
            super(...args);
            /// ....
        }
        someMethod() {}
    };
}
```
For mixins to work in TypeScript, we have to use a type argument for the constructor we're extending.
It's also necessary that this constructor accepts any arguments.
This means that, if we want to have any initialization logic, we should forward all arguments to `super`.
Of course, if we don't declare a constructor in the mixin, we don't have to worry about that.

On the other hand, we're not forced to declare the constructor returns `any`.
It's possible to constrain the base type, and if we do that, we gain access to the methods of that type:
```typescript
interface Greeter {
    greet(): void;
    greetMessage(): string;
}

function augmentedGreeter<C extends new (...args: any[]) => Greeter>(klass: C) {
    return class extends klass {
        greetTwice() {
            this.greet();
            this.greet();
        }

        greetMessage() {
            return `${super.greetMessate()}!!!`;
        }
    }
}

const MyGreeter = augmentedGreeter(class MyGreeter {
    greet() {
        console.log(this.getMessage());
    }

    greetMessage() {
        return 'Hello, world';
    }
});
```
This is a nice way of providing additional functionality while making the dependencies of that functionality explicit.
We can also intercept calls made to the original class, for example.

So this means that mixins in JavaScript are not simply a way of bypassing the single inheritance restriction.
It's common for APIs to provide base class which we can extend. In languages that support it, the classes are usually
marked abstract because they lack some implementations, which are left to the user.
Using mixins, we can switch this around. Our classes will still implemented the wholes, and the API will extend our class.

The difference is which class is able to intercept the other's methods and call super.
If we then need to intercept the resulting classes's methods, we can still extend it, so we get maximum flexibility.

```javascript
function mixin(klass) {
    return class extends klass {
        value() {
            return super.value() * 2;
        }
    }
}
class Base {
    value() {
        return 20;
    }
}

class Final extends mixin(Base) {
    value() {
        return super.value() + 2;
    }
}

console.log(new Final().value()); // prints 42
```

## Conclusion
There was a time when Object Oriented Programming was the proper way of developing software.
Fortunately, the funcitional programming principles are now in the spotlight and now composition tends to be
preferred over inheritance.
That doesn't mean we can't mix things up. JavaScript always supported both programming paradigms and high order
classes is a great tool for developing some APIs.
