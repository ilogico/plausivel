import body from "./mixing-it-up.md";
import type Page from "/Page.js";

const page: Page = {
  title: "Mixing it up",
  date: "2020-05-09",
  href: "mixing-it-up",
  lang: "en",
  type: "markdown",
  body,
};

export default page;
