<h1>Scalable Text</h1>
<time datetime="2020-04-28">April 28, 2020</time>

Browsers are great at laying out content. They were already good for laying out documents with the block formating mode.
Now, they're even better with Flexbox and Grid.

Boxes can grow and shrink according to available space and their own content.
But what's hard to do is the opposite: growing and shrinking text depending on the available size.
There's good reason for this, of course, but we might want to do it anyway.

There's a way to define a box size as a function of its font size, but not the other way around.
And even if you could do that, it's generally not possible to know how much space some text will occupy.
Mono spaced fonts take the same space for every character, but this is not true for other fonts.
Adding to that, we still have to take into account the letter spacing,
so even using JavaScript, it would still be hard to calculate the size the content will end up with.

One idea is to render the text, measure it, and change its font size so it then fits the size we want.
This is approach might prove problematic, since we're not sure if the letter spacing is relative to the font size or not.
We might also cause reflows, where the browser has to calculate the layout again.

But there are good examples of content that scales well.
Images can grow and shrink to fill the available space.
We can even control if they're allowed to change their aspect ratio, in case the box they're in has different proportions.

SVG scales even better, preserving its quality almost entirely (I say almost because some paths might begin showing the lines that compose them).

The good news is, not only can we embed SVG in HTML, we can also embed HTML in SVG using the `foreignObject` element.
When HTML is embeded in SVG, it becomes a part of the image and scales with it.

## Initial exploration

<code-demo>

```html
<style>
svg { border: thin solid cyan; }
div { display: inline-block; }
</style>
<svg viewBox="0 0 100 40" width="50%">
    <foreignObject width="100%" height="100%"><div>example text</div></foreignObject>
</svg>
<svg viewBox="0 0 100 40" width="100%">
    <foreignObject width="100%" height="100%"><div>example text</div></foreignObject>
</svg>

```

</code-demo>

We can see the SVG growing and the text within it growing as well. But the text doesn't quite fit the SVG.
It can be bigger (and wrap) or smaller, depending on the text and the font sizing.

In order to make it fit, we need to understand the SVG `viewBox` property.
This property determines the way the coordinates are interpreted inside the SVG viewport.

The first two numbers represent the coordinates of the top left corner, ie. the starting point of both axis.
We left them at 0, so we didn't have to specify where the `foreignObject` starts (the default is x="0" and y="0").

The next values represent the width and height of the viewport (before any scaling occurs).
We can use these to make the viewport fit the text, instead of making the text fit the viewport.
This works because we can still control the space the SVG will occupy by constraining its actual width and height.

So my next strategy is to use JavaScript to measure the text and change the SVG viewport to match it.

## A calculated approach

<code-demo>

<div style="display: none;">

```html
<style>
form {
    display: flex;
    align-items: start;
    flex-direction: column;
}
</style>
<form>
    <label>Mono font <input name="mono" type="checkbox"></label>
    <label>Font size (px) <input name="font-size" type="number" value="16"></label>
    <label>Container width (rem) <input name="container-width" value="20" type="number"></label>
    <label>Container height (rem) <input name="container-height" value="20" type="number"></label>
    <label>Text <input name="text" value="example text"></label>
</form>
```
</div>

```html
<style>
#output {
    width: max-content;
    height: max-content;
    margin: 0;
    padding: 0;
}
#container {
    font-size: 16px;
}
</style>
<div id="container">
    <svg>
        <foreignObject width="100%" height="100%"><div id="output">example text</div></foreignObject>
    </svg>
</div>

```

```css
svg {
    width: 100%;
    height: 100%;
    border: thin solid cyan;
}

#container {
    width: 20rem;
    height: 20rem;
}
```

```javascript
const output = document.getElementById('output');
const svg = document.querySelector('svg');

const { width, height } = output.getBoundingClientRect();
svg.setAttribute('viewBox', `0 0 ${output.offsetWidth || 1} ${output.offsetHeight || 1}`);
```

<div style="display: none;">

```javascript
const output = document.getElementById('output');
const container = document.getElementById('container');

document.querySelector('[name="mono"]')
    .addEventListener('input', e => container.style.fontFamily = e.target.checked ? 'mono' : '');

document.querySelector('[name="font-size"]')
    .addEventListener('input', e => container.style.fontSize = `${e.target.value}px`);

document.querySelector('[name="container-width"]')
    .addEventListener('input', e => container.style.width = `${e.target.value}rem`);

document.querySelector('[name="container-height"]')
    .addEventListener('input', e => container.style.height = `${e.target.value}rem`);

document.querySelector('[name="text"]')
    .addEventListener('input', e => output.textContent = e.target.value);

```
</div>
</code-demo>

I added some controls so we can see the effect of changings some CSS values and the text itself.

We can see the initial rendering (after the JavaScript corrected the `viewBox`) is correct.
Hopefully, no reflow occurs, when the `viewBox` is corrected, since both its width and its height is constrained by the container, which has fixed dimensions.


One thing you may notice is I inlined the style applied to the output element.
This is to ensure it's dimensions are correctly measured.
Even though the JavaScript is running after all the DOM loaded, the browser may apply CSS lazily, which is the behaviour I'm observing in Firefox and Chromium.
But of course, this is a hack I'll want to remove.
But `#output` CSS is important. The display is `inline-block` because we want it to be a single box (`inline` elements can have multiple boxes when the line breaks),
but we don't want it to occupy all horizontal space, otherwise we won't be able to mease its actual content.
The `max-content` on the minimum sizing prevents the content from wrapping.

So this strategy almost worked.
If we could assume the font properties never change, everything else is easy to fix.
Since we're likely the ones controlling when the text changes, we can recalculate the sizes right after the change.
Even if the content changes unexpectedly, we can still use a [MutationObserver](https://developer.mozilla.org/en-US/docs/Web/API/MutationObserver)
and guarantee we respond to change.

But we can do better than that, there's a new observer that joined the observer family:
the [ResizeObserver](https://developer.mozilla.org/en-US/docs/Web/API/ResizeObserver).
Safari will implement it soon, but it's already available on Firefox and Chromium.

## A more robust solution

The MutationObserver will allow us to react whenever our content changes size, so we can fix `viewBox` immediatly.

But there are other things we can improve if we want to make something reusable.
Using a custom element for this is perfect. For one, we hide all those wrappers (`svg`, `foreignObject`, etc.) inside the shadow DOM.
They don't belong in the real DOM anyway, since they are not real content.
Then, the solution becomes robust, since other CSS won't be able to interfere with the properties we found necessary for this to work.

<code-demo>
<div style="display: none;">

```html
<style>
form {
    display: flex;
    align-items: start;
    flex-direction: column;
}
</style>
<form>
    <label>Mono font <input name="mono" type="checkbox"></label>
    <label>Font size (px) <input name="font-size" type="number" value="16"></label>
    <label>Container width (rem) <input name="container-width" value="20" type="number"></label>
    <label>Container height (rem) <input name="container-height" value="20" type="number"></label>
    <label>Text <input name="text" value="example text"></label>
</form>
```
</div>

```html
<scalable-content id="result">example text</scalable-content>
```

```css
#result {
    width: 20rem;
    height: 20rem;
    font-size: 16px;
    border: thin solid cyan;
}
```

```javascript
const template = document.createElement('template');
template.innerHTML = `
<style>
    :host, slot {
        display: inline-block;
    }
    slot {
        min-width: max-content;
        min-height: max-content;
    }
    svg { width: 100%; height: 100%; }
</style>
<svg>
    <foreignObject width="100%" height="100%"><slot></slot></foreignObject>
</svg>
`;

class ScalableContent extends HTMLElement {
    constructor() {
        super();

        const content = template.content.cloneNode(true);
        const svg = content.querySelector('svg');

        const observer = new ResizeObserver(
            ([{ contentRect: { width, height } }]) => {
                svg.setAttribute(
                    'viewBox',
                    `0 0 ${width || 1} ${height || 1}`,
                );
            }
        );
        observer.observe(svg.querySelector('slot'));

        this.attachShadow({ mode: 'closed' }).append(content);
    }
}

customElements.define('scalable-content', ScalableContent);
```
<div style="display: none;">

```javascript
const result = document.getElementById('result');
document.querySelector('[name="mono"]')
    .addEventListener('input', e => result.style.fontFamily = e.target.checked ? 'mono' : '');

document.querySelector('[name="font-size"]')
    .addEventListener('input', e => result.style.fontSize = `${e.target.value}px`);

document.querySelector('[name="container-width"]')
    .addEventListener('input', e => result.style.width = `${e.target.value}rem`);

document.querySelector('[name="container-height"]')
    .addEventListener('input', e => result.style.height = `${e.target.value}rem`);

document.querySelector('[name="text"]')
    .addEventListener('input', e => result.textContent = e.target.value);

```
</div>
</code-demo>

This solution has a bit more JavaScript, even though some of it is really HTML and CSS.
Nevertheless, everything else got much better.
Whenever any property changes, the content readjusts immediatly to fit the constraints.
The component is very easy to use and works with other content too, not just text.
It's also possible to implement a resizable input using something like `<span contentEditable="true"></span>` inside.

SVG, HTML that scales :)
