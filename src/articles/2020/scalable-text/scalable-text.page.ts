import body from "./scalable-text.md";
import type Page from "/Page.js";

const page: Page = {
  title: "Scalable Text",
  href: "scalable-text",
  date: "2020-04-28",
  lang: "en",
  type: "markdown",
  body,
  modules: ["/webcomponents/code-demo.ts"],
};

export default page;
