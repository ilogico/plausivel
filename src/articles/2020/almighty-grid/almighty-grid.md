<h1>The almighty grid</h1>
<time datetime="2020-05-12">May 12, 2020</time>

CSS Grid is out there for a while now and I haven't had much change to use it.
I've been working on a React Native project for two years now, and it doesn't support grid.
And when I finally can get my hands on some web code, the dreaded Internet Explorer prevents using the grid in its
greatest potential.

I think Internet Explorer has only some of the blame in some of us not adopting the CSS grid.
We can always try to negotiate some tiered support for IE, leaving it simpler, but still usable layouts.

I believe one the other reasons we do this is because CSS is a broad and sometimes complicated subject,
so we collect small tricks to get things done.
I remember learning this one to achieve vertical alignment:

```css
.container { position: relative; }
.box {
  position: absolute;
  top: 50%;
  transform: translateY(-50%);
}
```

That's a lot of ammunition just vertical align a box. The main problem being we're not using the properties for
what they're meant.
If we now want to animate the box using a transform, we have to add the translation we're already performing.
And we did never meant to remove the box from normal flow to begin with, this is basically a huge hack for little benefit.

Flexbox was a huge improvement, and now we can use properties like `align-items` and `justify-content` which are
actually designed to fix these issues.
But flexbox is so powerful, we're also using it for more than it was designed to do.

```css
.container {
  display: flex;
  flex-wrap: wrap;
}
.container > * {
  flex-shrink: 0;
  width: 25%;
}
```

Flex wasn't designed to align items in both dimensions, but we do it anyway by fixing their size in one dimension
to a percentage of the available space. Sometimes we resort to `calc` to deduct the margins and it still works.
We just have to add a bunch of breakpoints to change the `25%` to `20%` or `calc(100% / 3)` or whatever so the amount of
items change when viewport grows or shrinks. Media queries mean we depend on the viewport size, not the actual available
space for our pseudo grid. And this is all sounding a bit strange when the technology is called Flexbox.

## Grid super power: fr

I think one of the habits we have to get rid of is sizing things.
We all learnt that pixels are bad, so we use other units instead.
But I feel like the problem isn't the unit, it's sizing itself.
Even when we use percentages, we have to take into account paddings and borders.
So we change the `box-sizing` to `border-box`.
Then we have a problem with margins and use `calc`, etc.

Grid allows us to simply say we want to distribute the space proportionally, we don't care about anything else:
```css
.container {
  grid-template-columns: repeat(4, 1fr);
}
```
This is very explicit: give me 4 equal sized columns.
It behaves like `flex-grow` but for the entire column, so everything is aligned.

So the elements themselves don't need to be sized, everything is controlled by the row/column templates.
If the items have the alignment/justification set to `stretch` (the default), they will grow to fill the space defined
by the grid. But we can always choose to center them instead, left-align them, etc.

## Grid super power: gap

The `grid-gap` property is actually being renamed to `gap`, as it's supposed to be implemented in `flex` as well,
but only Firefox implemented this so far. Only grid supports this in Chrome and Safari.
It's very common to need gutters in grids, so enough is enough, no more tricks with margins and removing margins to outer
elements with negative margins, etc.

We need to define space between cells? We use gap.

## Grid super power: auto placement

Trying to learn what we can do with grid, I went to several articles and videos.
It's very common to see the disclaimer "Grid is not a replacement for Flexbox".
It's like they're saying "We don't want to mess with your favourite tool".

Alright, Flexbox its uses, especially since it's much better supported than Grid.
But a I try to find more uses for the Grid, the more it seems to me that the Grid is a better fit than Flexbox in most
situations.

And the reason Grid can do what most of what Flexbox can do is auto placement.

I recently (finally) tried to understand what it means "Internet Explorer supports an older version of the specification".
I found that the most important missing funcionality was auto placement.

Auto placement is what allows us to add items to a grid without specifying their column or row.
Which means we can use the grid pretty much like a flex container, but with extra goodies like gap.
And using the `grid-auto-columns` or `grid-auto-rows`, we can determine the size of the items without applying any
CSS to them, like we would have to do to flex's children.

## Grid super power: auto-fit and minmax

For this one, I have to show this video: [auto-fit](https://gridbyexample.com/video/series-auto-fill-auto-fit/).

I heard about grid for the first time for some years now. Every now and then I looked it up again, but I had never seen
this wonder.

My initial reaction was "Look how many media queries this could've saved me", but it's not just that.
Media queries are very limited because they only allow us to write code that depends on the viewport sizes, not the actual
available space for the container.
A grid can be shown alone in one page and side by side in the other and the viewport will be the same.

I won't be able to get this out of my mind for some time. And I will definitely curse the next time I'll write media
queries to fix things like these.

## Grid super power: Named areas

A classical example of page layout is the header/sidebar/main/footer layout.
I don't mean to say this is the most common layout, I have no idea if it is or not.
But it's often used as an example.

It's fairly easy to implement it with Flexbox, but it is absolutely perfect for Grid:

```css
body {
  display: grid;
  margin: 0;
  min-height: 100vh;
  grid-template:
    "header  header header" auto
    "sidebar main   main"   1fr
    "footer  footer footer" auto
    / auto   1fr    1fr;
}
.header { grid-area: header; }
.sidebar { grid-area: sidebar; }
.main { grid-area: main; }
.footer { grid-area: footer; }
```

I believe on of the benefits of grid is we don't need to make artificial elements just to layout elements.
If I used `flex`, there was a good chance I had to wrap the sidebar and the main content inside some div.

But having these elements allocated by using area names it's an excelent extra.
If the designer now decides the sidebar should cover the footer as well, we only have to change the grid-template, nothing else.
We can even move the sidebar to the right without touching anything else.
This is actually a very bad idea because the tab order only cares about the source order, but it demonstrate the what we could do in other situations.

## Grid super power: flexible stacking

Until CSS Grid, we could stack elements on top of each other by using positioning.
But by positioning elements manualy, we forgo the browser's layout engine.
The positioned elements are either removed from normal flow (`absolute` and `fixed`), so they don't contribute
with their content when determining the parent's size, or they will still occupy their place in the flow (`relative` and `sicky`)
even though they might not be there.

With grid, we can have the best of both worlds. By placing elements manually in overlapping cells, they will occupy the same space.
Which elements end up on top, depend on the source order and `z-index` if any.

But the elements are still influenced by elements on the same rows and columns.
Here's a contrived example:

<code-demo>

```html
<div class="container">
  <div class="background" role="presentation"></div>
  <div class="header">
    Prepare garrets it expense windows shewing do an. She projection advantages resolution son indulgence. Part sure on no long life am at ever. In songs above he as     drawn to. Gay was outlived peculiar rendered led six.
  </div>
  <div class="footer">
  Advantage old had otherwise sincerity dependent additions. It in adapted natural hastily is justice. Six draw you him full not mean evil.
  </div>
  <div class="aside">
    On recommend tolerably my belonging or am. Mutual has cannot beauty indeed now sussex merely you. It possible no husbands jennings ye offended packages pleasant he. Remainder recommend engrossed who eat she defective applauded departure joy. Get dissimilar not introduced day her apartments.
  </div>
</div>
```
```css
body {
  margin: 0;
}

.container {
  display: grid;
  grid-template: auto auto / auto auto;
}

.background {
  grid-column: 1;
  grid-row: 1 / span 2;
  background: black;
}

.header, .footer {
  grid-column: 1;
  color: white;
}
.header {
  grid-row: 1;
  border-bottom: thin solid white;
}
.footer { grid-row: 2; }
```
</code-demo>

Overlapping elements must be manually placed, but the `.aside` element fits automatically in the first available cell.

We can also see the `.background` element is growing to fit the cells we allocate it, which are determined
by the contents of all the other elements, including the `.aside`.

## Grid future power: subgrid

I call this a future power because the only browser which implemented this is Firefox, the browser from the future.

I admit my examples are very sloppy and the previous one is a bit far fetched.
But it illustrates a common problem which is to align content across different elements.

One way we can do it is, for example, using `display: contents;`.
This display value works a bit like `none`, because no box is generated for the element.
But the element's children are still shown as if they were children of their grandparent.

This means we use elements for logical grouping, but pretend they don't exist for layout purposes.
The orphaned children can then become flex or grid items without being direct children.

While that approach might be reasonable sometimes, it's also often impossible.
Furthermore, an element with `display: contents;` will have it's background, border and padding removed.

Subgrid allows us to tackle this problem some CSS. It is not a new display value.
Subgrids are regular grids, the `subgrid` keyword is applied to the `grid-template-columns` or `grid-template-rows` properties.

If you're using Firefox, you can see it working in the following example:

<code-demo>

```html
<div class="container">
  <div class="item">
    <div class="header">
      Prepare garrets it expense windows shewing do an. She projection advantages resolution son indulgence. Part sure on no long life am at ever. In songs above he as     drawn to. Gay was outlived peculiar rendered led six.
    </div>
    <div class="footer">
    Advantage old had otherwise sincerity dependent additions. It in adapted natural hastily is justice. Six draw you him full not mean evil.
    </div>
  </div>
  <div class="item">
    <div class="header">Nothing to say</div>
    <div class="footer">
      On recommend tolerably my belonging or am. Mutual has cannot beauty indeed now sussex merely you. It possible no husbands jennings ye offended packages pleasant he. Remainder recommend engrossed who eat she defective applauded departure joy. Get dissimilar not introduced day her apartments.
    </div>
  <div>
</div>
```

```css
.container {
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  gap: 1rem;
}
.item {
  display: grid;
  grid-template-rows: subgrid;
  grid-row: span 2;
  background: black;
  color: white;
  border: thick solid orange;
  border-radius: 0.5rem;
  padding: 1rem;
}
```

</code-demo>

The code is very simple, we just make the items occupy 2 rows, and then make its children contribute to the outer.
The item still has its border, and padding, but all its children are aligned.
It's like eating a cake and have it too.

## Conclusion

Flexbox was a layout revolution, but we need the next one.
There are a lot of great resources out there on how to use CSS Grid by people who understand it far better than me.

My objective in writting this is to share my enthusiasm.
I believe this technology has a huge potential and we need to start making compromises with simpler layouts
on old browsers so we can let the modern ones shine.
