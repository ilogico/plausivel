import type Page from "/Page.js";
import body from "./almighty-grid.md";

const page: Page = {
  title: "The almighty grid",
  date: "2020-05-12",
  href: "almighty-grid",
  body,
  lang: "en",
  type: "markdown",
  modules: ["/webcomponents/code-demo.ts"],
};

export default page;
