import { h, render } from 'preact';
import { useState, useEffect } from 'preact/hooks';

function useDigit() {
  const [digit, setDigit] = useState(0);

  useEffect(
    () => {
      const timer = setInterval(
        () => setDigit(digit => (digit + 1) % 10),
        1000,
      );
      return () => clearInterval(timer);
    },
    [],
  );
  return digit;
}

function ConditionalDigit() {
  const digit = useDigit();
  const top = [0, 2, 3, 5, 6, 7, 8, 9].includes(digit);
  const middle = [2, 3, 4, 5, 6, 8, 9].includes(digit);
  const bottom = [0, 2, 3, 5, 6, 8].includes(digit);
  const topLeft = [0, 4, 5, 6, 8, 9].includes(digit);
  const bottomLeft = [0, 2, 6, 8].includes(digit);
  const topRight = [0, 1, 2, 3, 4, 7, 8, 9].includes(digit);
  const bottomRight = [0, 1, 3, 4, 5, 6, 7, 8, 9].includes(digit);

  return (
    <svg class="virtual-digit" viewBox="0 0 130 210">
      {top && <line x1="10" y1="5" x2="120" y2="5" />}
      {middle && <line x1="10" y1="105" x2="120" y2="105" />}
      {bottom && <line x1="10" y1="205" x2="120" y2="205" />}
      {topLeft && <line x1="5" y1="15" x2="5" y2="95" /> }
      {bottomLeft && <line x1="5" y1="115" x2="5" y2="195" />}
      {topRight && <line x1="125" y1="15" x2="125" y2="95" />}
      {bottomRight && <line x1="125" y1="115" x2="125" y2="195" />}
    </svg>
  );
}

function CSSDigit() {
  const digit = useDigit();

  return (
    <svg class="css-digit" data-digit={String(digit)} viewBox="0 0 130 210">
      <line class="top" x1="10" y1="5" x2="120" y2="5" />
      <line class="middle" x1="10" y1="105" x2="120" y2="105" />
      <line class="bottom" x1="10" y1="205" x2="120" y2="205" />
      <line class="top-left" x1="5" y1="15" x2="5" y2="95" />
      <line class="bottom-left" x1="5" y1="115" x2="5" y2="195" />
      <line class="top-right" x1="125" y1="15" x2="125" y2="95" />
      <line class="bottom-right" x1="125" y1="115" x2="125" y2="195" />
    </svg>
  );
}

render(
  <ConditionalDigit />,
  document.getElementById('virtual-digit')!,
);

render(
  <CSSDigit />,
  document.getElementById('css-digit')!,
);
