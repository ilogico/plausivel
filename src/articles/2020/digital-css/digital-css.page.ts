import type Page from "/Page.js";
import body from "./digital-css.md";

const page: Page = {
  title: "Digital CSS",
  lang: "en",
  href: "digital-css",
  date: "2020-05-15",
  type: "markdown",
  body,
  styles: ["./digital-css.css"],
  modules: ["./digital-css.module.tsx"],
};

export default page;
