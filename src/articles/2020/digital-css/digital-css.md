<h1>Digital CSS</h1>
<time datetime="2020-05-15">May 15, 2020</time>

I'm currently working on a project using CSS in JS.
Defining CSS in JS is increasingly popular.
This is isn't surprising, it looks like people don't like using multiple programming langues.
If you look at SQL, for example, you'll find a bunch of <abbr title="Object-relational mapping">ORM</abbr>
and query builder libraries for every language.

You might notice I'm calling SQL and CSS programming languages, which I think they are.
I heard it before: "SQL is not a programming language, it's a Query Language".
But we are using these languages to define the program's behaviour, so they're programming languages.
They are not general purpose programming languages, they are focused on solving a particular problem, which they solve
better than most other languages.

They are also declarative languages, so in a world where the word "declarative" has such much hype, I don't think we
should set them aside.

## A digital display

Digital systems are also declarative.
A display for showing digits, for example, might consist of 7 LEDs.
Each one of these LEDs is active when some numbers are selected, and disabled otherwise.

If we imagine we have 10 buttons, labeled from 0 to 9, when a button is pressed it activates a signal and the
corresponding LEDs are lit.

So, for example, the top LED being lit can be described like this: `b0 | b2 | b3 | b5 | b6 | b7 | b8 | b9`.
It's lit when any button other than 1 or 4 are pressed.

You maybe wondering what happens when multiple buttons are pressed. I'm not going to worry about that.
We could define that behaviour by having the signals cancel each other or some other behaviour.
But for this, exercise, I'll assume the LEDs for each button will be lit, like multple numbers are stacked on top of each other.

To emulate the display, I'm going to use a bit of SVG:

```html
<svg class="digital-digit" viewBox="0 0 130 210">
   <line class="top" x1="10" y1="5" x2="120" y2="5" />
   <line class="middle" x1="10" y1="105" x2="120" y2="105" />
   <line class="bottom" x1="10" y1="205" x2="120" y2="205" />
   <line class="top-left" x1="5" y1="15" x2="5" y2="95" />
   <line class="bottom-left" x1="5" y1="115" x2="5" y2="195" />
   <line class="top-right" x1="125" y1="15" x2="125" y2="95" />
   <line class="bottom-right" x1="125" y1="115" x2="125" y2="195" />
</svg>
```

I gave the lines classes so it's easier to understand what they're for.
Basically, each LED is represented by a `line` element. I'll then use some CSS to make the liners thicker, choose the color, etc.

## Using JavaScript to make it work

For this initial implementation, I'm going to use conditional rendering to display the digits.
Because using Virtual DOM libraries like React makes it so easy to render something conditionally, I feel this solution
is intuitive for many developers.

```jsx
function useDigit() {
  const [digit, setDigit] = useState(0);

  useEffect(
    () => {
      const timer = setInterval(
        () => setDigit(digit => (digit + 1) % 10),
        1000,
      );
      return () => clearInterval(timer);
    },
    [],
  );
  return digit;
}

function ConditionalDigit() {
  const digit = useDigit();
  const top = [0, 2, 3, 5, 6, 7, 8, 9].includes(digit);
  const middle = [2, 3, 4, 5, 6, 8, 9].includes(digit);
  const bottom = [0, 2, 3, 5, 6, 8].includes(digit);
  const topLeft = [0, 4, 5, 6, 8, 9].includes(digit);
  const bottomLeft = [0, 2, 6, 8].includes(digit);
  const topRight = [0, 1, 2, 3, 4, 7, 8, 9].includes(digit);
  const bottomRight = [0, 1, 3, 4, 5, 6, 7, 8, 9].includes(digit);

  return (
    <svg class="virtual-digit" viewBox="0 0 130 210">
      {top && <line x1="10" y1="5" x2="120" y2="5" />}
      {middle && <line x1="10" y1="105" x2="120" y2="105" />}
      {bottom && <line x1="10" y1="205" x2="120" y2="205" />}
      {topLeft && <line x1="5" y1="15" x2="5" y2="95" /> }
      {bottomLeft && <line x1="5" y1="115" x2="5" y2="195" />}
      {topRight && <line x1="125" y1="15" x2="125" y2="95" />}
      {bottomRight && <line x1="125" y1="115" x2="125" y2="195" />}
    </svg>
  );
}
```

<div id="virtual-digit"></div>

This could have been done differently. Instead of stating the function for each LED,
I could have a map like this:

```js
const map = {
  '0': {
    top: true,
    middle: false,
    bottom: true,
    topLeft: true,
    bottomLeft: true,
    topRight: true,
    bottomRight: true,
  },
  /* etc. */
};
```

But even if we omit the falsy values, it's still quite verbose. Using the `includes` we can avoid `or`ing to compare to
different numbers, so it's succint.

Now, this works, but I don't like it.
Conditionally rendering the LEDs removes possibilities.
We might want to apply different opacities to the LEDs instead.

Well, that could be fixed by applying classes conditionally to the elements, but we can do better than that.

## Moving the display logic to CSS

CSS has everything we need for this example
We can express `and` by concatenating selectors and we can express `or` by separating selectors with commas.

So we only need to find a way of communicating the selected number to CSS.
One possibility would be using the number as a class name, but `.0` isn't a valid selector.
Creating classes like `digit-0` would work, as would using attribute selectors `[class="0"]`.

The first one opens the possibility that our display can have multiple digits selected at the same time:
`digit-0 digit-1` is a possible class name. It's nice if our API can denote the way it's supposed to be used.

Using attribute selectors to select classes defeats the purpose of using classes in the first place:
`0 strong` is a valid class name, but `[class="0"]` only selects elements where the attribute is exactly `0`.

I think attribute selectors is the way to go, so I'll use custom attributes, also known as data attributes: `[data-digit="0"]`.

CSS can be a bit verbose because we can't (yet) factor out the common logic.
For example, in JS we can change `(a || b) && (a || c)` to `a && (b || c)`.
I think this is one of the reasons pre processors like SCSS became so popular.

So if want to express the display logic for the `bottom-left` (the one with less cases), I will have to do something like

```css
.css-digit[data-digit="0"] > .bottom-left,
.css-digit[data-digit="2"] > .bottom-left,
.css-digit[data-digit="6"] > .bottom-left,
.css-digit[data-digit="8"] > .bottom-left {
}
```

Now, there are some advantages in being explicit, but this is a lot of repetition.

There are two selectors that can help with this, but they aren't implemented in any browser: `:is()` and `:where()`.
They are equivalent in most situations, differing only in the way they affect the specificity of the query.

What browsers do implement has a prefix, a different name and incomplete, but will work for this example: `:-webkit-any()`
and `:-moz-any()`. I will use `:is()` anyway, just for the clarity of the example. But bear in mind the code has to be changed
to support current browsers:

```css
.css-digit:is([data-digit="0"], [data-digit="2"], [data-digit="5"], [data-digit="8"]) > .bottom-left {
}
```

This is a litle bit better while still being explicit.
But using the same strategy, we can also choose to specify the parts that are visibile for each digit:

```css
.css-digit > line {
  opacity: 0.1;
  transition: opacity 0.2s;
}
.css-digit[data-digit="0"] > :not(.middle) {
  opacity: 1;
}
.css-digit[data-digit="1"] > :is(.top-right, .bottom-right) {
  opacity: 1;
}
.css-digit[data-digit="2"] > :is(.top, .top-right, .middle, .bottom-left, .bottom) {
  opacity: 1;
}
.css-digit[data-digit="3"] > :is(.top, .top-right, .middle, .bottom-right, .bottom) {
  opacity: 1;
}
.css-digit[data-digit="4"] > :is(.top-left, .middle, .top-right, .bottom-right) {
  opacity: 1;
}
.css-digit[data-digit="5"] > :is(.top, .top-left, .middle, .bottom-right, .bottom) {
  opacity: 1;
}
.css-digit[data-digit="6"] > :not(.top-right) {
  opacity: 1;
}
.css-digit[data-digit="7"] > :is(.top, .top-right, .bottom-right) {
  opacity: 1;
}
.css-digit[data-digit="8"] > * {
  opacity: 1;
}
.css-digit[data-digit="9"] > :is(.middle, .top-left, .top, .top-right, .bottom-right) {
  opacity: 1;
}
```

I show all of the CSS here so it's a fair comparison with the previous implementation.
A lot of cases would've been easier to write if browsers properly supported the `:not` selector.
It's supposed to accept a list of comma separated selectors, but only Safari implements this correctly.
I suppose since most developers are using CSS pre-processors, browsers developers see this as a low priority.

You might find this a bit verbose, but I find it quite easy to understand.
And now that most of the display logic is moved to CSS, our JavaScript can look like this:

```jsx
function CSSDigit() {
  const digit = useDigit();

  return (
    <svg class="css-digit" data-digit={String(digit)} viewBox="0 0 130 210">
      <line class="top" x1="10" y1="5" x2="120" y2="5" />
      <line class="middle" x1="10" y1="105" x2="120" y2="105" />
      <line class="bottom" x1="10" y1="205" x2="120" y2="205" />
      <line class="top-left" x1="5" y1="15" x2="5" y2="95" />
      <line class="bottom-left" x1="5" y1="115" x2="5" y2="195" />
      <line class="top-right" x1="125" y1="15" x2="125" y2="95" />
      <line class="bottom-right" x1="125" y1="115" x2="125" y2="195" />
    </svg>
  );
}
```

<div id="css-digit"></div>

You can inspect this version in the developer tools and compare it to the other.
This tree is much more stable, with a single attribute changing.

## Conclusion

If you measure how adequate each programmin language is for a given task, you might be tempted to consider
the number of lines required to achieve that particular task.

While I admit that metric is important, there are other things to consider.

I find it easier to read and maintain code that has a clear separation of concerns.
While a lot can be achieved with conditional rendering, which is quite easy to do these days,
I find that simply applying styles using clever selectors is cleaner.

Having used React Native for the last 2 years, the most notable absence in the framework for me was CSS.
Yes, you can use objects that look like CSS, but you miss the ability to create complex logic using selectors.

I feel it's quite common to use CSS simply to describe the styles and then determine which ones will be applied exclusively through JS,
using class names as glue to bring it together.

Class names are a good way of identifying the elements you want to apply some styles, but not for identifying states.
While the built in pseudo-classes like `:focus` and `:checked` are a great way to identify states of `input`s, for example,
we can also use data attributes to achieve a similar result for our custom elements.

CSS code might be difficult to organize and maintain, but that's mainly because many times the display logic is
complicated. Components have multiple possible states and its different parts may need to react to those states.
We end up with components which have multiple variants, multiple states and on top of that we often need media queries as well.
As the combinations grow, it's tempting to move the logic to JavaScript, which allows the creation of powerful abstractions
to manage that.

However, moving CSS to JS might eventually infect the application logic its complexity.
My guess is developers will eventually give up on this idea, when this becomes apparent.
