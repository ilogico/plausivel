<h1>Uncontrolled forms</h1>
<time datetime="2020-05-16">May 16, 2020</time>

One of the best things about programming is that we're constantly learning.
We contact with new ideas from our colleagues and we learn new ways of thinking as we try to keep up with the newe technologies.

But sometimes developing software can also be tedious, we have to solve the same problems multiple times in different contexts.
Like many others, I try to cope with that trying to find newer and better abstractions.

A problem we often revisit is the `form` problem. The reason I think forms are hard is because I was never able to settle
into a single way of doing forms. No abstraction seems powerful enough to handle all cases.

I know there are a lot of libraries that claim they provide a clean way of handling forms, but I was never convinced they
actually helped, most of the times I feel they only make things worse.

## The ReactJS way

ReactJS has two distinct ways of handling form inputs: controlled and uncontrolled.
I'll call the first one the React way for a few reasons: it became popular with React, it's the only way in
React Native and the uncontrolled variant is basically what we get using the browser APIs.

Controlled inputs are inputs where the value is determined the application.
When the user types something, we detect it and update the application state, which is reflected back to the input.
If choose not to update the application state, React will reset the input to its previous value.

Now, this is a really clever idea and makes the application state to be consistent with values the inputs hold.
I always found it a bit of an overkill to constantly re-render the form whenever the user types, but this approach has its
merits. We can keep track of the input validity all the time, so we can disable or enable buttons in real time, for example.

One of the things I can't understand in React is why its developers decided to rename the `input` event to `change`.
It's true there might be some confusion as to which one should be used, but I like it that both are available
and I think it's harmful to make a generation of developers believe the `change` event is triggered as the user types.

Anyway, React needed to set up this abstraction and now we can use the `onChange` handler on text inputs and checkboxes
instead of using `onInput` for one and `onChange` for the other.

But there's another problem with controlled inputs that React "fixes" for us: if the form has a reset button, neither
`change` or `input` events are triggered. In fact, to handle to the form reset, we have to listen to the `reset` event.
So React fixed this by crippling the ability to reset forms. When we set an input's value in React, it synchronizes
the `value` attribute as well. But the `value` attribute doesn't map to the input's `value` property, it maps to its
`defaultValue` property. This makes sense because if you write this HTML `<input value="something">`, you're not
making the input read only, you're just setting its default value.

So what the reset button does is changing the input's value (which has no representation in attributes) to the input's default value.
By updating the input's default value as the user types, React makes the reset event a no op.

I feel this is an ugly hack and it was one of the reasons I know prefer Preact.
Substituting one problem with the other, I prefer to have the solution closest to how the browser behaves.

This React behaviour is actually a security problem. Synchronizing the default input's value makes it more visible in
the DOM. Mixpanel, and analytics tool, ended up collecting these attributes. This included password fields and other sensible information.

## Letting the forms be free

When HTML5 was a buzzword, everyone talked about HTML5 inputs. We don't hear about it so much these days.

It turned out, they weren't a complete solution to the form problem.
I think this understandable. There are many opinions on how the forms should behave and this changes over time.
Also, it's common to do asynchronous validations and HTML5 doesn't help much there.

The way these APIs were designed made some assumptions about how forms were being used:
the inputs are validated when the user tries to submit the form, if there are errors, the browser points them out
using some styles we can't really control.

There are also other problems. There is some confusion about what type is for date pickers (datetime? datetime-local?).
And the date pickers have different styles across browsers and operating systems and we have no way of overriding them.

The input type `number` also tends to be confusing because some people use it for strings, which happen to consist
of digits only, but are not numbers. The credit card number, for example, is a string, not a number. If you don't do math
with it, it's not a number. I'm not saying this is a problem with the API itself, but I believe it contributed to
people drifting way from it. (btw, inputs have an attribute called `inputmode` which you can use to hint which virtual keyboard
to use, so it's possible to have keyboard suitable for numbers without the input being of type `number`)

Another confusing point is the `required` attribute. This attribute only prevents the input from being completely empty,
but you can still submit the form with white space. On the other hand, if you define an input of type `email`,
the form can be submitted when the input is empty. While I don't think this is a design mistake
(except maybe for allowing empty space only required fields), I think it also adds a bit to the confusion.

Having said all of this, I think HTML5 inputs are a great improvements.
They are semantic and accessible, they provide pseudo-classes we can target with CSS and have pretty decent JavaScript APIs.

All of this has really nothing to do with inputs being controlled or not. I think we should use this attributes regardless
of the way we choose to handle forms. But I also think having too much focus on JavaScript makes developers forget the browser
can do things on its own, we don't need to control every aspect of the user interaction.
Since most forms are not actually submitted, but processed through JavaScript, sometimes people even forget to make the
form, they just write inputs and buttons.

So, if you want to use these attributes, but you don't want the browser to interfere at all, you can simply add the
the `novalidate` attribute to the form. This means you want to add semantic value to the inputs, but you want to do the validations yourself.

Another approach is to add an event listener to the `invalid` event and call the `preventDefault`.
Preventing the default will stop the browser from showing any message to the use, but you can still use the browser's
default message, it's available in the target's `validationMessage` property.
Bear in mind the `invalid` event doesn't bubble, so to listen to it at the form level, you have to use capture mode.
In React, this means you have to use the `onInvalidCapture` handler.

Most of the times, we'll want to use our own custom error messages. We can still benefit from the browser's validations
if we choose to do so. Inputs have a `validity` property which let's inspect if the input is in a valid state and which validations
are failing. `validity` is an object with several properties. If `valid` is `true`, then all other properties are `false`.
If `valid` is `false`, at least one other property is `true`.

This object is always up to date, it doesn't require the user to try to submit the form.
So if you have a required field, and you're listening to the `input` event, you can check the `event.target.valid` and the
`event.target.valueMissing` and then set your error accordingly without waiting for an `invalid` event.
But you can still trigger and `invalid` event but calling the `checkValidity()` method on the input.

Having the browser making the validations for you saves you some work, but it also makes the input more accessible.
Another advantage is makes the `:invalid` pseudo class available, so you can apply styles in CSS without making up class names.

Now, `required` and `type="email"` are fairly easy to type, but we often need much more custom validations.
There's this `pattern` attribute we can use to supply a regex the browser uses to validate the input.
Unlike many developers, I actually love writing regular expressions. But I have to admit they have limited potential in form
validation. They are great for preventing white space only inputs, for example, but for a regular expression for
validating if a password is at least 8 characters, has a lower and upper case letter and digit is pain.

Furthermore, in many cases, the validity of one input depends on the value of other inputs (passwords must match).
But in all these cases, you can signal the browser the input is in an invalid state by calling it's `setCustomValidity()`
method. It takes a string argument, which will be used to override the `validationMessage` property.
When you detect the input becomes valid again, you can simply call the same method with an empty string.

Letting the browser know the input is invalid will once again let you tap into the `:invalid` selector.
But unfortunately, if you're using React, you can't forward this information using props.
You can either call it immediatly when listening to the `input` event
(which means you're validating on the event instead of the render, I think this is fine).
Or you can use a `useLayoutEffect` (or `useEffect` if you prefer, but it looks like a property I want to bundle with the rendering stage),
in which case you'll need a *ref* to the input.

## What about the data?

Alright, so we can do form validation without controlling the inputs, even if it means listening to the input changes
anyway.
But the main advantage of controlled components is keeping track of the data and making sure the data our application has
matches the input's.

To get this right, we're kind of forced to use the right tools.
- All inputs should have a name, so we do that.
- All inputs should have their validity synched at all times.
- We listen to the `submit` event on the form, not the click of buttons.

If we followed these rules, in our `submit` event, the target is the form.
The form has a `checkValidity()` method which will return true if the form is valid,
and false otherwise. This includes invalid states created with `setCustomValidity`.

The form will also have an `elements` property which allows us to access elements by name.
But I would say the best way to get the data is using the `FormData` API.
If you do `new FormData(event.target)` on the `submit` event, you'll get an object containing the form data (as the name implies).
This object can actually be used in a fetch request if you're using `urlencoded` forms, but since it's more likely you'll need
a JSON representation of the data, it's necessary to extract the data manually with `.entries`, `.has`, `.get` and `.getAll`.

It's true we have to some conversions in this part. For example, for checkboxes, we'll have to check the form has the value,
rather than just using it. But for most cases, we would have to some conversions anyway when using controlled inputs.
For example, if input really represents a number, we probably need to retain the value the user actually typed
(imagine the user is typing a negative number, we can't simply convert `-` to a number) and at some point we need to
convert it to the number wee need in our JSON representation.

This `FormData` API has been here for a while, it's even supported in IE10.
But it's also an important part of an upcoming API (already supported in Firefox and Chromium browsers) which will allow
custom form controls. Basically, using the `FormData` constructor on a form, will trigger an event which allows
it's children to contribute data do the `FormData`.
So instead of just having `input`, `textarea` and `select` contributing values to the form, we might have some custom elements
behaving like inputs in the future.

## Conclusion

While having the application keeping track of all of the user's input, might be a good idea,
it can also be cumbersome.
An alternative is to only track the validity of inputs, not their actual values, which are most useful when the form is submitted.

We can validate inputs in a custom way, but sometimes we can delegate this responsability to the browser.
Letting the browser know about the validity of the form improves accessibility and lets us use the `:invalid` selector in CSS.

In most cases, focusing on validity is the right thing because it's what's really controlling the application state.
It's very likely we want to disable the input because the email is not valid, we don't care about which particular email the user entered.

As a bonus, validity and error messages change less often than user input, so letting the inputs be free will also makes
our application do less useless re-rendering.
