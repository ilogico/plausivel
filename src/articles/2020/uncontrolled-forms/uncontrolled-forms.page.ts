import body from "./uncontrolled-forms.md";
import type Page from "/Page.js";

const page: Page = {
  title: "Uncontrolled forms",
  lang: "en",
  date: "2020-05-17",
  href: "uncontrolled-forms",
  type: "markdown",
  body,
};

export default page;
