import { ComponentChildren, createContext, h, render } from 'preact';
import { useCallback, useContext, useMemo, useState } from 'preact/hooks';
import { getMessage } from './initial-commit.lib.js';

document.querySelector('#javascript-target')?.append('Adding this text with plain DOM manipulation: ', getMessage());

interface ClickService {
    readonly clicks: number;
    addClick: () => void;
}
const ClickContext = createContext<ClickService>({
    clicks: 0,
    addClick: () => { },
});

function AppController({ children }: { children: ComponentChildren }) {
    const [clicks, setClicks] = useState(0);
    const addClick = useCallback(() => setClicks(clicks => clicks + 1), []);
    const value = useMemo<ClickService>(() => ({ clicks, addClick }), [clicks]);

    return (
        <ClickContext.Provider value={value}>{children}</ClickContext.Provider>
    );
}

function ClickButton() {
    const clickService = useContext(ClickContext);

    return <button type="button" title="Add a click" onClick={clickService.addClick}>{clickService.clicks}</button>;
}

function App() {
    return (
        <AppController>
            <section>
                <header>This is rendered by <del>React</del> <ins>Preact</ins></header>
                <ClickButton />
            </section>
        </AppController>
    )
}

render(
    <App />,
    document.getElementById('react-root')!,
);
