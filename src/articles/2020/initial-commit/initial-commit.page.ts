import body from "./initial-commit.md";
import type Page from "/Page.js";

const page: Page = {
  title: "Initial Commit",
  href: "initial-commit",
  date: "2020-03-28",
  lang: "en",
  type: "markdown",
  body,
  modules: ["./initial-commit.module.tsx"],
  styles: ["./initial-commit.css"],
};

export default page;
