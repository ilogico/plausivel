import body from "./sliding-in-the-shadow.md";
import type Page from "/Page.js";

const page: Page = {
  title: "Sliding in the shadow",
  href: "sliding-in-the-shadow",
  date: "2020-05-05",
  lang: "en",
  type: "markdown",
  body,
  modules: ["/webcomponents/code-demo.ts"],
};

export default page;
