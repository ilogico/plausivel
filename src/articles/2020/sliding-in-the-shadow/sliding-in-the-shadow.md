<h1>Sliding in the shadow</h1>
<time datetime="2020-05-05">May 5, 2020</time>

In this post I will create a Custom Element to demonstrate the benefits of the Shadow DOM.

One of the problems web developers have been struggling with since forever is CSS encapsulation.

CSS was designed with the idea that the appearance of the document should not be bound to the content itself.
This makes sense if you consider the content may be consumed in several different ways (screen readers, crawlers, etc.),
so we don't know if the styles are even relevant.

So instead of styles, the elements should have ids (which are event relevant for navitation) and semantic class names,
something like `featured` for example.

With higher demand for more complex layouts, CSS frameworks appeared so we began to see `col-md-6`, `float-left`,
or `hide-on-xs grd-cell bg-content ctr-container`. This is basically writting styles inline, but using the class
attribute instead of style. It's a pervertion of the separation of concerns, which becomes evident when someone asks you
to change the style of some component: you're more likely to have to edit HTML than CSS.

As CSS evolved, the need for these frameworks were reduced and CSS pre-processors grew.
As websites also grew in complexity, development with components became dominant.
And to increase the modularity of components, developers started using CSS modules:
class names are tranformed to something unique, so we can be sure our CSS rules won't apply to unwanted elements.
So now we have class names that look like this: `_daa751 _9dc2ec`.

In this model, we still have separation of concerns. These names are meaningless, but in the the source code, they
probably map to something like `selected` `category`.

But even if we don't mind having meaningless class names in the document, there are still problems with this approach:
1. There is no true encapsulation. Our components are only safe from being targeted by class name, but they still can
be selected by all the other CSS selectors, which we might be tempted to do because of the following problem.
2. There is no standard way of styling components from the outside. Since the parts of the component (and the component itself)
have names we can't guess, we can't apply styles to them directly. We either try to target them with a different selector
like `button:first-child`, which forces us to know the inner workings of the component, breaking modularity and risking breakage
when the component's implementation changes; or we can develop components so that they accept additional class names as arguments,
which we add to the specific parts of the component.

This last approach might be usable enough, but it feels a bit clumsy to me to have to define styles in CSS, import the class name in JS,
only to forward it to every child component where we want the style to apply. And it only works if the style is to be applied
to a component we're configuring. If I want to style the `FancyButton`, but I'm only rendering the `FancyForm`,
the latter would have to accept styles for it's own parts and it's children parts, etc.

Now, CSS in JS is trending and some of the solutions used for JavaScript components are being transfered to the CSS realm.
Things like React Context can be used to forward styles and class names to sub children.
We were able to keep the styles out of the HTML, but it seems very hard to keep them apart from JS.

## Enter the shadow

The Shadow DOM API is part of the Web Components initiative.
It introduces a method in HTML elements named `attachShadow`.
When this method is called, the element gains a shadow root, which is like a document fragment or a mini document.
Elements which have a shadow root display the contents of the shadow root instead of their own contents.

The reason this is useful is that elements within the shadow root cannot be targeted by the "outside" CSS.

Additionaly, we can add stylesheets to the shadow root, and those styles will not apply to the outside world.
The stylesheets can be added using a `<style />` element or linking to an external stylesheet with a `<link />` element.
Chrome already implemented an additional way of attaching stylesheets called [Constructable Stylesheets](https://wicg.github.io/construct-stylesheets/).

So this solves the encapsulation problem.

## We only want part of it

I mentioned another problem earlier, which is being able to style parts of the components.
This is solved by a new HTML attribute: `part`. When you add this attribute to an element, it becomes targetable by the outside CSS
using the `::part()` selector. So when you're authoring a component, you can decide which parts are themable and provide a clean API for it.
The styles can be applied through CSS only.

## Insert slots

The `<slot />` element provides a way of teleporting content from the outside DOM to the shadow DOM, adding templating capabilities to it.

```html
<my-component>
    <div slot="first">0</div>
    <div>1</div>
    <div slot="first">2</div>
</my-component>
```

```js
class MyComponent extends HTMLElement {
    constructor() {
        super();
        this.attachShadow({ mode: 'closed' }).innerHTML = `
        <slot name="first">first</slot>
        <slot>second</slot>
        <slot name="third">third</slot>
        `;
    }
}
customElements.define('my-component', MyComponent);
```

In the example above, the browser will render:

0<br>2<br>0<br>third

So the elements which were assigned to the first slot are rendered inside the slot with that name.
The `div` labeled 1 goes to the default (nameless) slot.
The slot name `third` is assigned no elements, so it displays its default content: the text `third` in this case.

It's important to understand that the elements assigned to slots never leave the outside DOM.
They are rendered in the layout of the shadow DOM, but they are still outside and can be targeted by the regular DOM.

This also means we can apply the styles we want in the shadow DOM, without fear of interfering with any elements we adopted.

However, there might be cases where we actually need to apply styles to the elements we adopted. And that's what the `::slotted()` selector is for.

The `HTMLSlotElement` interface also provides the `assignedElements` and `assignedNodes` methods to access the adopted children and
a `slotchange` event to detect changes to them.

## And finally we slide

<code-demo>

```html
<style>
    my-slider {
        max-width: 30rem;
        border: 0.25rem solid gold;
    }

    my-slider::part(previous-button) {
      border: none;
      background: black;
      color: white;
    }

    svg {
      width: max-content;
    }
</style>
<my-slider>
    <span slot="next-button">go go go</span>
    <svg viewBox="0 0 100 100">
        <circle cx="50" cy="50" r="25" fill="olivedrab" />
    </svg>
    <svg viewBox="0 0 100 100">
        <rect x="25" y="25" width="50" height="50" fill="rebeccapurple" />
    </svg>
    <svg viewBox="0 0 100 100">
        <polygon points="25,25 75,25 50,75" fill="salmon" />
    </svg>
    <svg viewBox="0 0 100 100">
        <path d="M25,75 S25,50,50,25 75,50,75,75 50,100,25,75z" fill="gold" />
    </svg>
    <svg viewBox="0 0 100 100">
        <path d="M25,75 S50,50,50,25 50,50,75,75 50,50,25,75z" fill="lime" />
    </svg>
</my-slider>
```

```javascript
const template = document.createElement('template');
template.innerHTML = `
<style>
:host {
  display: inline-block;
}

#content {
  display: grid;
  width: 100%;
  grid-auto-columns: 100%;
  grid-auto-flow: column;
  overflow-x: scroll;
  scroll-snap-type: x mandatory;
  justify-items: center;
  align-items: center;
}

#content::slotted(*) {
  max-width: 100%;
  scroll-snap-align: center;
}

#container {
  position: relative;
  width: 100%;
  height: 100%;
}

button {
  position: absolute;
  min-width: 2rem;
  top: 0;
}

[part="previous-button"] { left: 0; }
[part="next-button"] { right: 0; }


</style>
<div id="container">
<button part="previous-button"><slot name="previous-button">previous</slot></button>
<slot id="content"></slot>
<button part="next-button"><slot name="next-button">next</slot></button>
</div>
`

class MySlider extends HTMLElement {
  constructor() {
    super();
    const shadowRoot = this.attachShadow({ mode: 'closed' });
    shadowRoot.appendChild(template.content.cloneNode(true));

    const content = shadowRoot.getElementById('content');
    let selectedIndex = -1;
    const observer = new IntersectionObserver(
      entries => entries.forEach(entry => {
        if (entry.intersectionRatio > 0.5) {
          selectedIndex = content.assignedElements().indexOf(entry.target);
        }
      }),
      {
        threshold: 0.5,
        root: content,
      },
    );

    const observedElements = new Set(content.assignedElements());
    observedElements.forEach(e => observer.observe(e));

    content.addEventListener('slotchange', () => {
      const newElements = new Set(content.assignedElements());
      observedElements.forEach(e => {
        if (!newElements.has(e)) observer.unobserve(e);
      })

      newElements.forEach(e => {
        if (!observedElements.has(e)) {
          observer.observe(e);
          observedElements.add(e);
        }
      });

      if (selectedIndex >= observedElements.size) selectedIndex = observedElements.size - 1;
    });

    shadowRoot.querySelector('[part="previous-button"]').addEventListener('click', () => {
      if (selectedIndex > 0) {
        content.assignedElements()[--selectedIndex].scrollIntoView({ behavior: 'smooth' });
      }
    });

    shadowRoot.querySelector('[part="next-button"]').addEventListener('click', () => {
      const elements = content.assignedElements();
      if (selectedIndex < elements.length - 1) {
        elements[++selectedIndex].scrollIntoView({ behavior: 'smooth' });
      }
    });
  }
}

customElements.define('my-slider', MySlider);
```

</code-demo>

The example might be a little contrived, but it shows off the features I described above.

Since the snapping mechanism is pure CSS, I'm using an `IntersectionObserver` to detect which element is currently in view.
This is important so that the buttons know which element to scroll to when they're clicked.
We could also use this to dispatch a `change` event. Since we need to observe the elements, we need to react to the changes, hence the `slotchange` event.

I provided a way to change the content of the buttons through named slots and also a way to style the button itself,
so we can hide them, position them, etc.
If we don't assign elements to these named slots, the fallback applies, so the buttons remain functional.

Inside the shadow DOM, it's safe to use ids, since they're not going to colide with any ids outside.
We can even target elements by their tag name, it won't affect any adopted children.
But since we need to specify `scroll-snap-align` on the adopted elements to make the CSS snap work, we use the `::slotted(*)` selector
to apply it. There's a limitation though, it's impossible to target bare text nodes.
If you insert text in the slider, it will be assigned to the default slot, since it's impossible to give it the slot attribute.
Since I'm using CSS grid for the layout, the text nodes will be assigned a grid cell, so the layout isn't broken.
But because there is no way to apply the `scroll-snap-align` to these anonymous elements, it will be impossible to actually stop the scroll at those locations.

## Conclusion

While libraries like React, Vue and Angular are very good at mapping data and state to content, they don't provide such a clear way of managing styles.
Custom elements with shadow DOM are very good at encapsulating styles, while providing a standard way of overring the styles for parts of the component.
Shadow DOM also has very good templating capabilities making it well suited for constructing wrappers or layout components,
even hiding extra elements from the regular DOM.

I look forward for a more semantic web, with all the clutter locked in the shadows.
