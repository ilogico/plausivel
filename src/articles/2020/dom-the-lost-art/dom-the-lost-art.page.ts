import body from "./dom-the-lost-art.md";
import type Page from "/Page";

const page: Page = {
  title: "DOM: the lost art",
  href: "dom-the-lost-art",
  date: "2020-05-06",
  lang: "en",
  type: "markdown",
  body,
};

export default page;
