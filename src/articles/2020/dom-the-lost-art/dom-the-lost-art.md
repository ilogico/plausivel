<h1>DOM: the lost art</h1>
<time datetime="2020-05-06">May 6, 2020</time>

When I started learning web development, most people seemed to be using jQuery and its plugins for everything.
If you searched for how to do something, there was a very good change you would find dolar signs in the solution.
jQuery had its time, and now that I think about it, it was actually a great library for what people were doing then.

However, I always tried to understand how the native web APIs worked. I think it was a good strategy.
Libraries and frameworks come and go, but the browser APIs can't go away (otherwise the web would break).

The DOM and browser APIs are standardized and fairly well documented.
However, newer web developers don't usually have a chance of learning them because they skip directly to
higher level libraries or frameworks.

Since libraries don't need to be standardized, so their API surface can grow freely to accommodate additional functionality,
increasing productivity in many cases. They are also useful for abstracting differences between browsers, although in many
cases, we can also use polyfills for this.

But even knowing most developers prefer to use libraries, I was still surprised to find that many experienced and
highly productive and competent developers had barely no contact with the browser APIs.

That's why I decided to write about it.

## The tree

DOM stands for Document Object Model, which is basically means it's a way to represent the document using objects.

These objects are nodes of a tree data structure, which means some of these nodes have child nodes and a node can have
at most one parent node.

Nodes can be of different types, but they all extend the [Node](https://developer.mozilla.org/en-US/docs/Web/API/Node) interface.

The document itself is also a node, even though we rarely treat as such. But if we inspect its `childNodes` property,
we'll usually find 2 nodes: one is the doctype declaration (which is also represented as node) and the `<html />` element.

The document structure is rigid, it will always have an `<html />` node, which is accessible at the `documentElement`
property. This element always has exactly 2 children: the `<head /` and `<body />` elements.
Even if these aren't written explicitly on the HTML source, the browser creates them and makes them available at the
document's `head` and `body` properties.

Elements are the also nodes, and they are the nodes we manipulate more often. They can have attributes
(which are actually another kin of node, even if we don't need to see them as such) and they can have child nodes.
These nodes always extends the [Element](https://developer.mozilla.org/en-US/docs/Web/API/Element) interface,
but they are usually instances of a specific class, depending on their tag name. For example, input elements are instances
of the `HTMLInputElement` class. All HTML elements extend the `HTMLElement` class and all SVG elements extend the `SVGElement` class.

Text in the page is represented through yet another kind of node, the text node. These nodes cannot have child nodes
and instead have a property `nodeValue` which holds the string they contain. These nodes are instances of the class
[Text](https://developer.mozilla.org/en-US/docs/Web/API/Text), which extends another called `CharacterData`.

Another type of `CharacterData` is `Comment`, whose objects represent comments in the document. Yes, comments are
part of the DOM!

There are other types of nodes, but these will cover our needs for now.

## Building the tree

So how do these nodes come into existence?

There are essentially two ways to create nodes: using HTML or using JavaScript.

### HTML
Using HTML is the obvious way, but the results can be a bit surprising:

```html
<div>
    <span>Hello, <!----> world!</span>
</div>
```

This creates a `div` element, which has 3 direct child nodes. Yes, 3.
If you inspect the `children` property, it contains one element. But that's because that property only includes children which are elements.
Inspecting the `childNodes` property reveals 3 nodes: `[#text, <span />, #text]`. The first and the last nodes contain
the white space (new lines and spaces) surrounding the `span`.

The `span` also has 3 children: 2 text nodes and 1 comment node.
The browser tries to merge all text into a single text node, if possible. But because we're inserting a comment in the middle,
the browser is forced to create 2 different text nodes and a comment node.
(this is actually used by some virtual DOM frameworks when rendering things server side, so that the virtual nodes can be recreated client side).

### JavaScript

JavaScript provides a few ways of constructing nodes.

One is simply using HTML again. Elements have the dreaded `innerHTML` propery.
When this property is set, the browser will remove all of the elements children first, and then add the ones that resulted from parsing the supplied string.

```javascript
var div = document.getElementById('some-div-id');
div.innerHTML = "<span>Hello</span> world!</span>";
```

In the example above, all of the div's child nodes would be removed and replaced by 2 nodes: a span and a text node.
The resulting span would contain a single text node.

Setting the `outerHTML` property would replace the `div` instead changing its contents.

Both of these properties are very dangerous. The string will be treated as HTML and if it's constructed using input we don't control,
there's a serious security vulnerability. Bear in mind scripts are injected in pages using `script` tags.

```js
div.innerHTML = '<script src="https://malicious.com/evil.js"></script>';
```

If scripts are injected in the page, they will have access to the user's stored credentials, forge requests, etc.

A much safer approach is setting the `innerText` or `textContent` properties.
These have slightly different behaviours when used as getters, but they do the same thing when used as setters:
remove all the node's children and add a single text node containing the supplied string.
Even if the string contains HTML, it will not be interpreted as such, not elements will be created, just a single text node.

Using `innerHTML` with strings you write is fine in what security is concerned, but it has other drawbacks.
One is that you can only replace the element's children, you can't add nodes to the existing ones.
And another is that parsing HTML is somewhat expensive. HTML is very irregular (some elements can omit closing tags in certain situations, for example),
so parsing it isn't as straight forward as it might seem.

Nodes can also be created more programatically. The standard way of creating elements is calling the `document.createElement('div')`
method. This will create an instance of `HTMLDivElement`, but it's not possible to call the constructor directly, as it will throw a `TypeError`.
(Custom elements can be created either using the constructor or with `document.createElement`)

SVG elements, on the other hand, must be created with `document.createElementNS`.
`NS` stands for Name Space, this function takes a first argument which is a string identifying the name space.
For SVG, this string is `http://www.w3.org/2000/svg`. Bear in mind that some tag names can either represent SVG or HTML
elements, depending on the way they're constructed or the HTML context they appear in the source. This is the case of `a` and `script`, for example.
If you create elements which the browser doesn't recognize as HTML elements, they will be instances of `HTMLUnknownElement`.

Text nodes and comments can be created using their constructors, but there are also the `document.createTextNode('some text')`
and the `document.createComment('some comment')` methods.

After creating these nodes, they can be programatically modified and populated.
The `appendChild` is particulary useful for adding nodes at the end and the `insertBefore` allows us to insert a node before another child.
With the `removeChild`, it's possible to remove any node from it's parent, dettaching it from the rest of the tree.

More modern methods include `append`, `prepend`, `before`, `after`, `remove` and `replaceWith`.

Methods that add child nodes can operate on nodes which are already connected to a tree, in which case the are previously removed
and then reattached. This allows us to move nodes to a different parent or reorder nodes  within a parent
(like ReactJS does when we have a list with keys and the order changes).

Setting atributes can be done using `setAttribute` and they can also be removed with the `removeAttribute` method.
For commonly used attributes, there are usually getters and setters. These getters and setters usually have the name of the attribute they set,
but it's not always the case. `class` for example couldn't be used as a property name in earlier versions of JS, so it was renamed to `className`.
With the `for` attribute of the `label` element, the same thing happened, so it was renamed to `htmlFor`.

There are other discrepencies as well. For example, the `value` attribute of the `input` element maps to the `defaultValue` property,
while the `value` property represents an input's state, with no HTML representation.
Similarty the `defaultChecked` property maps to the attribute `checked`. But in this case, setting it with a truthy value
will set the `checked` attribute to an empty string, while setting it to a falsy value will remove the attribute entirely.

Attributes are necessarily strings, but there elements have utility props for manipulating common ones in a more structured way.

The `class` attribute for example, can actually hold any number of classes separated by spaces. So elements have a `classList` property
which is array like (has length, can be indexed, has a `forEach` method) and can be used to `add`, `remove` and `toggle` individual classes.

The `style` attribute is also a string, but the `style` property is actually an object of type `CSS2Properties`.
Individual styles can be set using this object, which will be reflected in the `style` attribute.
The style names are transformed from the conventional CSS *kebak-case* to the *camelCase*.

A similar name transformation occurs for the customizable data attributes. An attribute named `data-some-data`
will be accessible at the elements `dataset.someData`.

Putting it all together, constructing a bit of DOM can look like this:

```javascript
const div = document.createElement('div');
div.appendChild(document.createTextNode('Hello, '));
const span = document.createElement('span');
span.appendChild(document.createTextNode('World'));
span.className = 'world';
div.appendChild(span);
document.body.appendChild(div);
```

As you can see, it's all imperative and very verbose.
The above code only produces what can be achieved through HTML with `<div>Hello, <span class="world">World</span></div>`.
So it's no surprise developers use libraries or fallback to `innerHTML` for this.
But it's also very fast, so most libraries use these methods under the hood.

## Document fragments

Another node type worth mentioning the `DocumentFragment`.
This is a utility node created to contain other nodes, so it can represent a piece of document.
Fragments can be created using the constructor or using the `document.createDocumentFragment` method.
Other nodes can be added as if this was an element.

But if a document fragment is appended to another node, it's children are appended instead and fragment becomes empty.
So fragments can never actually be a part of the document, they are simply a subtree which is always detached from the document,
but it can readily be appended to other nodes.

Document fragments are an important part of the `template` element API.
This element is part of the Web Components initiative.

The template element behaves differently when we set its `innerHTML`.
Instead of adopting the resulting nodes as its children, the template puts them in a document fragment accessible at its
`content` property.

So we can use this element essentially as an HTML parser.

But in order to be able to reuse these fragments, we have to clone them (remember the nodes are moved when the fragment is appended).
Fortunately, there's a `cloneNode` method on all nodes that does just this and it accepts a boolean which tells the method to recursively clone the children as well.

Since we can clone nodes, we can construct a template using HTML once and then clone them as many times as we want without incurring the HTML parsing penalty.

```javascript
const template = document.createElement('template');
template.innerHTML = `<div>Hello, <span class="world">World</span></div>`;
document.body.append(
    template.content.cloneNode(true),
    template.content.cloneNode(true),
);
```

Of course, this is a very rudimentary templating solution.
Dynamic content still needs to be changed after cloning.
You might need to add event listeners, etc.
But cloning nodes is a fast way of reproducing DOM trees and it has its uses.
I've been using this method to populate the shadow DOM of custom elements.

Another interesting thing when parsing HTML with the template element is that its elements are inert.
When you set the `src` attribute of an `img` element, for example, the browser will fetch the URL assuming you'll need it soon.
But this won't happen when parsing with the template element.

## Conclusion

There is a long gap between using the DOM directly and using libraries and frameworks.
But while manipulating the document using the browser's native methods is verbose and tedious, understanding the DOM is
not that complicated and gives us valueable insights into how the web works.
I think it's perfectly fine for a web developer to be interested in creating great products without the need to explore
these lower level APIs. But I also think being able to play with the DOM, even if it's just in the browser's console
gives us a comforting understanding of the big puzzle which is the web.
