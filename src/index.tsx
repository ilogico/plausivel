import type Page from "./Page.js";
import { Fragment, h } from "preact";

export default function Index({ pages }: { pages: readonly Page[] }) {
  const groups = groupPagesByMonth(pages);

  return (
    <Fragment>
      <h1>Plausível</h1>
      <ul className="months">
        {[...groups].map(([month, pages]) =>
          <Month key={month} month={month} pages={pages} />
        )}
      </ul>
    </Fragment>
  );
}
const monthFormatter = new Intl.DateTimeFormat(
  "en-GB",
  { month: "long", year: "numeric" },
);

function Month({ month, pages }: { month: string; pages: readonly Page[] }) {
  return (
    <li className="month">
      <time dateTime={month}>{monthFormatter.format(new Date(month))}</time>
      <ul className="articles">
        {pages.map((page) =>
          <Article key={`${page.date} ${page.title}`} page={page} />
        )}
      </ul>
    </li>
  );
}

function Article({ page }: { page: Page }) {
  return (
    <li className="article">
      <a href={`./${page.href}`}>
        <article>
          {page.title}
          <time dateTime={page.date}>
            {new Date(page.date).toISOString().slice(0, 10)}
          </time>
        </article>
      </a>
    </li>
  );
}

function groupPagesByMonth(pages: readonly Page[]) {
  const groups = new Map<string, Page[]>();

  pages.forEach((page) => {
    const key = page.date.slice(0, 7);
    const group = groups.get(key) ?? (() => {
      const group: Page[] = [];
      groups.set(key, group);
      return group;
    })();
    group.push(page);
  });

  return groups;
}
