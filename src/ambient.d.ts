declare module 'rollup-plugin-sourcemaps' {
    import { Plugin } from 'rollup';
    function rollupSourceMaps(): Plugin;
    export = rollupSourceMaps;
}

declare module '*.md' {
    const html: string;
    export default html;
}
