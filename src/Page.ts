import type { ComponentChildren } from 'preact';

interface BasePage {
    readonly title: string;
    readonly href: string;
    readonly date: string;
    readonly lang: string;
    readonly tags?: readonly string[];
    readonly modules?: readonly string[];
    readonly styles?: readonly string[];
}

interface MarkDownPage extends BasePage {
    readonly type: 'markdown';
    readonly body: string;
}

interface HTMLPage extends BasePage {
    readonly type: 'html';
    readonly body: ComponentChildren;
}

type Page = MarkDownPage | HTMLPage;

export default Page;
