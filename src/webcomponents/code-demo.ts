const template = document.createElement('template');
template.innerHTML = `
<style>
#container {
    display: grid;
    justify-items: stretch;
}
iframe {
    min-height: 60vh;
    background: white;
    border: none;
}
</style>
<div id="container"><slot></slot><iframe part="document"></iframe></div>
`;


class CodeDemo extends HTMLElement {
    constructor() {
        super();

        const shadowDOM = this.attachShadow({ mode: 'closed' });
        shadowDOM.appendChild(template.content.cloneNode(true));
        const iframe = shadowDOM.querySelector('iframe')!;

        const updateIframe = () => {
            const html = [...this.querySelectorAll('.language-html')].map(e => e.textContent).join('\n');
            const styles = [...this.querySelectorAll('.language-css')]
                .map(e => `<link rel="stylesheet" href="data:text/css;utf-8,${encodeURIComponent(e.textContent!)}">`)
                .join('\n');
            const modules = [...this.querySelectorAll('.language-js, .language-javascript')]
                .map(e => `<script type="module" src="data:text/javascript;utf-8,${encodeURIComponent(e.textContent!)}"></script>`)
                .join('\n');

            const document = `
                <!doctype html>
                ${styles}
                ${modules}
                <body>${html}</body>
            `;
            iframe.src = `data:text/html;utf-8,${encodeURIComponent(document)}`;
        };

        updateIframe();
        new MutationObserver(updateIframe).observe(this, { characterData: true, childList: true, subtree: true });
    }
}

customElements.define('code-demo', CodeDemo);

export {}
