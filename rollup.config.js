/// <reference lib="esnext" />
/// @ts-check

import nodeResolve from "@rollup/plugin-node-resolve";
import typescript from "@rollup/plugin-typescript";
import { readdirSync } from "fs";
import { readFile } from "fs/promises";
import highlightjs from "highlight.js";
import marked from "marked";
import * as path from "path";

export default {
  input: ["src/generatePages.tsx", ...find("src", /\.page\.tsx?$/)],
  output: {
    dir: "obj",
  },
  plugins: [
    nodeResolve({ preferBuiltins: true, jail: "src" }),
    typescript(),
    markdown(),
  ],
};

/**
 * @param {string} dir
 * @param {RegExp} pattern
 * @returns {string[]}
 */
function find(dir, pattern) {
  return readdirSync(dir, { withFileTypes: true })
    .flatMap((entry) => {
      const fullName = path.resolve(dir, entry.name);
      if (entry.isFile()) {
        return pattern.test(fullName) ? [path.resolve(dir, fullName)] : [];
      }

      if (entry.isDirectory()) {
        return find(path.resolve(dir, fullName), pattern);
      }

      return [];
    });
}

function markdown() {
  return {
    name: "load-markdown",
    async load(/** @type {string} */ id) {
      if (!id.endsWith(".md")) return null;

      const markdown = await readFile(id, "utf-8");
      const html = marked(
        markdown,
        {
          highlight(code, lang) {
            return highlightjs.highlight(lang, code).value;
          },
        },
      );

      return `export default ${JSON.stringify(html)};`;
    },
  };
}
