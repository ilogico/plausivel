#!/bin/sh
set -e

# clean
rm -rf public/*

# compile pages
npm run compile

# process pages
mkdir -p public
node obj/generatePages.js
cp static/* public/
find public -type f -exec gzip -k {} +
